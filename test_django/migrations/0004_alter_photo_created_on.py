# Generated by Django 3.2.3 on 2021-08-20 12:37

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('test_django', '0003_alter_photo_created_on'),
    ]

    operations = [
        migrations.AlterField(
            model_name='photo',
            name='created_on',
            field=models.DateTimeField(auto_created=datetime.datetime(2021, 8, 20, 12, 37, 13, 790584, tzinfo=utc), blank=True, null=True),
        ),
    ]
