import logging
import os
from django.db.models import Q
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse_lazy
from django.views import View
from django.views.generic import UpdateView, DeleteView
from .forms import CommentForm, ImageForm, AlbumForm
from hitcount.views import HitCountDetailView
from .models import *
import datetime

now = datetime.datetime.now()  


def homePage(request):
    albums = Album.objects.all()
    return render(request, 'index.html', {"albums": albums})


def albumPage(request, album_id):
    album = Album.objects.get(id=(album_id))
    photos = Photo.objects.filter(album=(album_id))
    return render(request, 'album_page.html', {'photos': photos, 'album': album})


def addPhotoPage(request):
    if request.method == 'GET':
        album = Album.objects.get(id=request.GET.get('album_id'))
        return render(request, 'add_photo.html', {'album': album})
    elif request.method == 'POST':
        if 'photo' not in request.FILES:
            return HttpResponse("File not specified", status=400)

        file = request.FILES['photo']
        dest_file_name = os.path.join(settings.MEDIA_ROOT, file.name)
        exist_file = Photo.objects.get(image=dest_file_name, album=request.POST['album'])
        if exist_file or os.path.exists(dest_file_name):
             return HttpResponse("File already exist", status=400)
        dest_file = open(dest_file_name, 'wb')

        if file.multiple_chunks():
            for chunk in file.chunks():
                dest_file.write(chunk)
        else:
            dest_file.write(file.read())

        dest_file.close()

        try:
            album = Album.objects.get(id=request.POST['album'])
            author = request.user
            description = request.POST['description']
            created_on = now.strftime("%Y-%m-%d %H:%M:%S")
            Photo.objects.create(image=dest_file_name, album=album, description=description, author=author,
                                 created_on=created_on).create_tags()
        except Exception as e:
            print(e)
        return redirect(f"/album-page/{request.POST['album']}")


def deletePhoto(request, pk):
    photo = Photo.objects.get(id=(pk))
    if request.method == "POST":
        photo.delete()
        return redirect('/')

    context = {'photo': photo}
    return render(request, 'delete_photo.html', context)


def addAlbumPage(request):
    if request.method == 'GET':
        return render(request, 'add_album.html')
    elif request.method == 'POST':
        if 'cover' not in request.FILES:
            return HttpResponse("File not specified", status=400)

        file = request.FILES['cover']

        dest_file_name = os.path.join(settings.MEDIA_ROOT, file.name)
        exist_file = Photo.objects.get(image=dest_file_name, album=request.POST['album'])
        if exist_file or os.path.exists(dest_file_name):
            return HttpResponse("File already exist", status=400)
        dest_file = open(dest_file_name, 'wb')

        if file.multiple_chunks():
            for chunk in file.chunks():
                dest_file.write(chunk)
        else:
            dest_file.write(file.read())

        dest_file.close()

        try:
            name = request.POST['name']
            description = request.POST['description']
            Album.objects.create(name=name, cover=dest_file_name, description=description)
        except Exception as e:
            print(e)

        return redirect('/')


def edit_album(request, pk):
    album_id = Album.objects.get(id=int(pk))
    form = AlbumForm(instance=album_id)

    if request.method == 'POST':
        form = ImageForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/')

    context = {'form': form}
    return render(request, 'edit_album.html', context)


class PhotoForm(LoginRequiredMixin, HitCountDetailView):
    model = Photo
    count_hit = True

    def get(self, request, pic_id, *args, **kwargs):
        id_pic=request.GET.get(id)
        photo = Photo.objects.get(id=pic_id)
        photo.photo_views = photo.photo_views + 1
        photo.save()
        form = CommentForm()
        try:
            comments = Comments.objects.filter(photo=photo).order_by('-created_on')

            notification = Notification.objects.create(notification_type=2, from_user=request.user,
                                                       to_user=photo.author,
                                                       photo=photo)
            stuff = get_object_or_404(Photo, id=self.kwargs['pk'])
            total_likes = stuff.total_likes()
        except Comments.DoesNotExist:
            comments = None
        context = {
            'photo': photo,
            'form': form,
            'comments': comments,

        }

        return render(request, 'pictureOpen.html', context)

    @login_required(login_url='login')
    def post(self, request, pic_id, *args, **kwargs):
        
        photo = Photo.objects.get(id=pic_id)
        form = CommentForm(request.POST)

        if form.is_valid():
            new_comment = form.save(commit=False)
            new_comment.author = request.user
            new_comment.photo = photo
            new_comment.save()
            new_comment.create_tags()
        comments = Comments.objects.filter(photo=photo).order_by('-created_on')
        stuff=get_object_or_404(Photo,id=self.kwargs['pk'])
        total_likes=stuff.total_likes()
        context = {
            'photo': photo,
            'form': form,
            'comments': comments,
            'total_likes':total_likes,
        }

        return render(request, 'pictureOpen.html', context)


class UpdatePhotoForm(UpdateView):
    model = Photo
    template_name = 'update_photo.html'
    fields = ['description', 'created_on']


def updatePhoto(request, pk):
    photo = Photo.objects.get(id=int(pk))
    form = ImageForm(instance=photo)

    if request.method == 'POST':
        form = ImageForm(request.POST, instance=photo)
        if form.is_valid():
            form.save()
            return redirect('/')

    context = {'form': form}
    return render(request, 'update_photo.html', context)


class AddDislike(LoginRequiredMixin, View):
    def post(self, request, pk, *args, **kwargs):
        post = Photo.objects.get(pk=pk)

        is_like = False

        for like in post.likes.all():
            if like == request.user:
                is_like = True
                break

        if is_like:
            post.likes.remove(request.user)

        is_dislike = False

        for dislike in post.dislikes.all():
            if dislike == request.user:
                is_dislike = True
                break

        if not is_dislike:
            post.dislikes.add(request.user)

        if is_dislike:
            post.dislikes.remove(request.user)

        next = request.POST.get('next', '/')
        return HttpResponseRedirect(next)


class AddCommentLike(LoginRequiredMixin, View):
    def post(self, request, pk, *args, **kwargs):
        comment = Comments.objects.get(pk=pk)

        is_dislike = False

        for dislike in comment.dislikes.all():
            if dislike == request.user:
                is_dislike = True
                break

        if is_dislike:
            comment.dislikes.remove(request.user)

        is_like = False

        for like in comment.likes.all():
            if like == request.user:
                is_like = True
                break

        if not is_like:
            comment.likes.add(request.user)

        if is_like:
            comment.likes.remove(request.user)
            notification = Notification.objects.create(notification_type=1, from_user=request.user,
                                                       to_user=comment.author, comment=comment)

        next = request.POST.get('next', '/')
        return HttpResponseRedirect(next)


class AddCommentDislike(LoginRequiredMixin, View):
    def post(self, request, pk, *args, **kwargs):
        comment = Comments.objects.get(pk=pk)
        is_like = False

        for like in comment.likes.all():
            if like == request.user:
                is_like = True
                break

        if is_like:
            comment.likes.remove(request.user)

        is_dislike = False

        for dislike in comment.dislikes.all():
            if dislike == request.user:
                is_dislike = True
                break

        if not is_dislike:
            comment.dislikes.add(request.user)

        if is_dislike:
            comment.dislikes.remove(request.user)

        next = request.POST.get('next', '/')
        return HttpResponseRedirect(next)


class CommentDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = Comments
    template_name = 'comment_delete.html'

    def get_success_url(self):
        pk = self.kwargs['pic_id']
        return reverse_lazy('pictureOpen', kwargs={'pic_id': pk})

    def test_func(self):
        photo = self.get_object()
        return self.request.user == photo.author


class CommentReplyView(LoginRequiredMixin, View):
    def post(self, request, pic_id, pk, *args, **kwargs):
        photo = Photo.objects.get(pk=pic_id)
        parent_comment = Comments.objects.get(pk=pk)
        form = CommentForm(request.POST)

        if form.is_valid():
            new_comment = form.save(commit=False)
            new_comment.author = request.user
            new_comment.photo = photo
            new_comment.parent = parent_comment
            new_comment.save()
        next = request.POST.get('next', '/')
        return redirect(f"/picture-open/{pic_id}/")
        notification = Notification.objects.create(notification_type=2, from_user=request.user,
                                                   to_user=parent_comment.author, comment=new_comment)

        return redirect('pictureOpen', pic_id=pic_id)


class ProfileView(View):
    def get(self, request, pk, *args, **kwargs):
        profile = UserProfile.objects.get(pk=pk)
        user = profile.user
        photos = Photo.objects.filter(author=user)

        followers = profile.followers.all()

        if len(followers) == 0:
            is_following = False

        for follower in followers:
            if follower == request.user:
                is_following = True
                break
            else:
                is_following = False

        number_of_followers = len(followers)

        context = {
            'user': user,
            'profile': profile,
            'photos': photos,
            'number_of_followers': number_of_followers,
            'is_following': is_following,
        }

        return render(request, 'profile.html', context)


class ProfileEditView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = UserProfile
    fields = ['name', 'bio', 'birth_date', 'location', 'picture']
    template_name = 'profile_edit.html'

    def get_success_url(self):
        pk = self.kwargs['pk']
        return reverse_lazy('profile', kwargs={'pk': pk})

    def test_func(self):
        profile = self.get_object()
        return self.request.user == profile.user


class AddFollower(LoginRequiredMixin, View):
    def post(self, request, pk, *args, **kwargs):
        profile = UserProfile.objects.get(pk=pk)
        profile.followers.add(request.user)
        notification = Notification.objects.create(notification_type=3, from_user=request.user, to_user=profile.user)
        return redirect('profile', pk=profile.pk)


class RemoveFollower(LoginRequiredMixin, View):
    def post(self, request, pk, *args, **kwargs):
        profile = UserProfile.objects.get(pk=pk)
        profile.followers.remove(request.user)
        return redirect('profile', pk=profile.pk)


class UserSearch(View):
    def get(self, request, *args, **kwargs):
        query = self.request.GET.get('query')
        profile_list = UserProfile.objects.filter(
            Q(user__username__icontains=query)
        )
        context = {
            'profile_list': profile_list,
        }
        return render(request, 'search_user.html', context)


class ListFollowers(View):
    def get(self, request, pk, *args, **kwargs):
        profile = UserProfile.objects.get(pk=pk)
        followers = profile.followers.all()
        context = {
            'profile': profile,
            'followers': followers,
        }
        return render(request, 'followers_list.html', context)


class PostNotification(View):
    def get(self, request, notification_pk, post_pk, *args, **kwargs):
        notification = Notification.objects.get(pk=notification_pk)
        post = Photo.objects.get(pk=post_pk)
        notification.user_has_seen = True
        notification.save()
        return redirect('pictureOpen', pk=post_pk)


class FollowNotification(View):
    def get(self, request, notification_pk, profile_pk, *args, **kwargs):
        notification = Notification.objects.get(pk=notification_pk)
        profile = UserProfile.objects.get(pk=profile_pk)
        notification.user_has_seen = True
        notification.save()
        return redirect('profile', pk=profile_pk)


class RemoveNotification(View):
    def delete(self, request, notification_pk, *args, **kwargs):
        notification = Notification.objects.get(pk=notification_pk)
        notification.user_has_seen = True
        notification.save()
        return HttpResponse('Success', content_type='text/plain')


class FindChatRoom(LoginRequiredMixin, View):
    def get(self, request):
        return render(request, 'chat/index.html')


class ChatDialogue(LoginRequiredMixin, View):
    def get(self, request, room_name):
        room = ChatRoom.objects.filter(name=room_name).first()
        chats = []

        if room:
            chats = Chat.objects.filter(room=room)
        else:
            room = ChatRoom(name=room_name)
            room.save()

        return render(request, 'chat/room.html', {'room_name': room_name, 'chats': chats})


@ login_required
def favourite_list(request):
     new = Photo.newmanager.filter(favourites=request.user)
     return render(request,
                   'accounts/favourites.html',
                   {'new': new})


 @ login_required
 def favourite_add(request, id):
     post = get_object_or_404(Post, id=id)
     if post.favourites.filter(id=request.user.id).exists():
         post.favourites.remove(request.user)
     else:
         post.favourites.add(request.user)
     return HttpResponseRedirect(request.META['HTTP_REFERER'])

@login_required
def like(request):
    if request.POST.get('action') == 'post':
        result = ''
        id = int(request.POST.get('postid'))
        photo = get_object_or_404(Photo, id=id)
        if photo.likes.filter(id=request.user.id).exists():
            photo.likes.remove(request.user)
            photo.like_count -= 1
            result = photo.like_count
            photo.save()
        else:
            photo.likes.add(request.user)
            photo.like_count += 1
            result = photo.like_count
            photo.save()

        return JsonResponse({'result': result, })
