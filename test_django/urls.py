from django.conf.urls.static import static
from django.urls import path
from .views import *

urlpatterns = [
    path('', homePage, name='gallery'),
    path('album-page/<int:album_id>/', albumPage, name='albumPage'),
    path('addphoto/', addPhotoPage, name='addphoto'),
    path('picture-open/<int:pic_id>/', PhotoForm.as_view(), name='pictureOpen'),
    path('profile/<int:pk>/', ProfileView.as_view(), name='profile'),
    path('profile/edit/<int:pk>/', ProfileEditView.as_view(), name='profile-edit'),
    path('picture-open/<int:pic_id>/comment/<int:pk>/reply', CommentReplyView.as_view(), name='comment-reply'),
    path('picture-open/<int:pic_id>/comment/delete/<int:pk>/', CommentDeleteView.as_view(), name='comment-delete'),
    path('add-album/', addAlbumPage, name='add_album'),
    path('picture-open/<int:pk>/dislike', AddDislike.as_view(), name='dislike'),
    path('update_photo/<str:pk>/update', updatePhoto, name="update_photo"),
    path('delete_photo/<str:pk>/', deletePhoto, name="delete_photo"),
    path('edit_album/<int:pk>/', edit_album, name="edit_album"),
    path('profile/<int:pk>/followers/', ListFollowers.as_view(), name='list-followers'),
    path('profile/<int:pk>/followers/add', AddFollower.as_view(), name='add-follower'),
    path('profile/<int:pk>/followers/remove', RemoveFollower.as_view(), name='remove-follower'),
    path('picture-open/<int:photo_pk>/comment/<int:pk>/like', AddCommentLike.as_view(), name='comment_likes'),
    path('picture-open/<int:photo_pk>/comment/<int:pk>/dislike', AddCommentDislike.as_view(), name='comment_dislikes'),
    path('search/', UserSearch.as_view(), name='profile-search'),
    path('notification/<int:notification_pk>/post/<int:post_pk>', PostNotification.as_view(), name='post-notification'),
    path('notification/<int:notification_pk>/profile/<int:profile_pk>', FollowNotification.as_view(),
         name='follow-notification'),
    path('notification/delete/<int:notification_pk>', RemoveNotification.as_view(), name='notification-delete'),
    path('chat/', FindChatRoom.as_view(), name='index'),
    path('chat/<str:room_name>/', ChatDialogue.as_view(), name='room'),
    path('like/', like, name='like'),

]
