from django.contrib import admin
from .models import *


# Register your models here.
# admin.site.register(Photo)
class PhotoAdmin(admin.ModelAdmin):
    # list_display = ('image','album','id')
    # fields = ('image', 'album')
    pass


class AlbumAdmin(admin.ModelAdmin):
    # list_display = ('image','album','id')
    # fields = ('name', 'cover', 'description')
    pass


#
# admin.site.register(Album, AlbumAdmin)
# admin.site.register(Photo, PhotoAdmin)

admin.site.register(Album)
admin.site.register(Photo)
admin.site.register(Comments)
admin.site.register(UserProfile)
admin.site.register(Notification)
# admin.site.register(Tag)
