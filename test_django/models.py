import datetime

from django.contrib.auth.decorators import login_required
from django.core import signals
from django.db import models
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.signals import request_finished
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from django.utils import timezone
from django.dispatch.dispatcher import receiver
from django.db.models.signals import *
import uuid

# from imagekit.models import ImageSpecField
# from imagekit.processors import ResizeToFill

User = get_user_model()


class Album(models.Model):
    name = models.TextField(max_length=1000)
    cover = models.ImageField(upload_to=settings.MEDIA_ROOT)
    description = models.TextField()
    # cover1 = ImageSpecField([
    #         ResizeToFill(200, 200)], source='cover',
    #         format='JPEG', options={'quality': 90})


@receiver(pre_delete, sender=Album)
def mymodel_delete(sender, instance, **kwargs):
    instance.cover.delete(False)


class Photo(models.Model):
    image = models.ImageField(upload_to=settings.MEDIA_ROOT)
    album = models.ForeignKey('Album', on_delete=models.CASCADE)
    description = models.TextField()
    created_on = models.DateTimeField(auto_created=timezone.now(), blank=True, null=True)
    author = models.ForeignKey(User, null=True, on_delete=models.CASCADE, db_constraint=False)
    likes = models.ManyToManyField(User, related_name='like', default=None, blank=True)
    like_count = models.BigIntegerField(default='0')
    favourites = models.ManyToManyField(
        User, related_name='favourite', default=None, blank=True)
    dislikes = models.ManyToManyField(User, blank=True, related_name='dislikes')
    photo_views = models.IntegerField(default=0, null=True, blank=True)
    tags = models.ManyToManyField('Tag', blank=True)

    # def __str__(self):
    #     return self.description

    # def total_likes(self):
    #     return self.likes.count()

    def create_tags(self):
        for word in self.description.split():
            if (word[0] == '#'):
                tag = Tag.objects.filter(name=word[1:]).first()
                if tag:
                    self.tags.add(tag.pk)
                else:
                    tag = Tag(name=word[1:])
                    tag.save()
                    self.tags.add(tag.pk)
                self.save()

    def was_published_recently(self):
        now = timezone.now()
        return now - datetime.timedelta(days=1) <= self.created_on <= now

    class Meta:
        ordering = ['-created_on']


class Comments(models.Model):
    comment = models.TextField()
    created_on = models.DateTimeField(auto_now_add=True)
    photo = models.ForeignKey('Photo', related_name='comments', on_delete=models.CASCADE)
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    likes = models.ManyToManyField(User, blank=True, related_name='comment_likes')
    dislikes = models.ManyToManyField(User, blank=True, related_name='comment_dislikes')
    parent = models.ForeignKey('self', on_delete=models.CASCADE, blank=True, null=True, related_name='+')
    tags = models.ManyToManyField('Tag', blank=True)

    def create_tags(self):
        for word in self.comment.split():
            if (word[0] == '#'):
                tag = Tag.objects.get(name=word[1:])
                if tag:
                    self.tags.add(tag.pk)
                else:
                    tag = Tag(name=word[1:])
                    tag.save()
                    self.tags.add(tag.pk)
                self.save()

    def __str__(self):
        return '%s-%s' % (self.photo.description, self.author)

    @property
    def children(self):
        return Comments.objects.filter(parent=self).order_by('-created_on').all()

    @property
    def is_parent(self):
        if self.parent is None:
            return True
        return False


class UserProfile(models.Model):
    user = models.OneToOneField(User, primary_key=True, verbose_name='user', related_name='profile',
                                on_delete=models.CASCADE)
    name = models.CharField(max_length=30, blank=True, null=True)
    bio = models.TextField(max_length=500, blank=True, null=True)
    birth_date = models.DateField(null=True, blank=True)
    location = models.CharField(max_length=100, blank=True, null=True)
    picture = models.ImageField(upload_to='uploads/profile_pictures', default='uploads/profile_pictures/default.png',
                                blank=True)
    followers = models.ManyToManyField(User, blank=True, related_name='followers')


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        UserProfile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()


class Notification(models.Model):
    # 1 = Like, 2 = Comment, 3 = Follow
    notification_type = models.IntegerField()
    to_user = models.ForeignKey(User, related_name='notification_to', on_delete=models.CASCADE, null=True)
    from_user = models.ForeignKey(User, related_name='notification_from', on_delete=models.CASCADE, null=True)
    photo = models.ForeignKey('Photo', on_delete=models.CASCADE, related_name='+', blank=True, null=True)
    comment = models.ForeignKey('Comments', on_delete=models.CASCADE, related_name='+', blank=True, null=True)
    date = models.DateTimeField(default=timezone.now)
    user_has_seen = models.BooleanField(default=False)


class Tag(models.Model):
    name = models.CharField(max_length=255)


class Chat(models.Model):
    content = models.CharField(max_length=1000)
    timestamp = models.DateTimeField(auto_now=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    room = models.ForeignKey('ChatRoom', on_delete=models.CASCADE)


class ChatRoom(models.Model):
    name = models.CharField(max_length=255)
